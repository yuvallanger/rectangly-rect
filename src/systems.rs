//! ---- SPECS systems

use crate::{
    components::Position,
    constants,
    resources::{
        GameOver, JumpIsDown, PlayerPosition, PlayerVelocity, TicksToNextJump, TicksToNextPipePair,
    },
};
use quicksilver::geom::Vector;
use rand::Rng;
use specs::prelude::{Entities, Join, Read, ReadStorage, System, Write, WriteStorage};

pub struct SystemPrintState;
pub struct SystemUpdatePosition;
pub struct SystemSpawnPipePair;
pub struct SystemDeletePipesIfOffScreen;
pub struct SystemBumpScreenEdge;
pub struct SystemBumpPipePair;
pub struct SystemPlayerJump;
pub struct SystemPlayerGravity;
pub struct SystemPlayerVelocity;

impl<'a> System<'a> for SystemPrintState {
    type SystemData = (Entities<'a>, ReadStorage<'a, Position>);
    fn run(&mut self, (entities, positions): Self::SystemData) {
        for (entity, position) in (&entities, &positions).join() {
            debug_value!("entity: {:?}; position: {:?}", entity, position);
        }
    }
}
impl<'a> System<'a> for SystemUpdatePosition {
    type SystemData = (WriteStorage<'a, Position>, Read<'a, GameOver>);
    fn run(&mut self, (mut position_write, game_over): Self::SystemData) {
        {
            if game_over.0 {
                return;
            }
        }
        for pos in (&mut position_write).join() {
            pos.0 += -Vector::new(constants::PIPE_PAIR_VELOCITY, 0.);
        }
    }
}
impl<'a> System<'a> for SystemSpawnPipePair {
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, Position>,
        Write<'a, GameOver>,
        Write<'a, TicksToNextPipePair>,
    );
    fn run(
        &mut self,
        (entities, mut position, game_over, mut ticks_to_next_pipe_pair): Self::SystemData,
    ) {
        if game_over.0 {
            return;
        }

        if ticks_to_next_pipe_pair.0 <= 0 {
            let new_pipe_pair = entities.create();
            let mut rng = rand::thread_rng();

            let new_position = rng.gen_range(
                constants::LOW_PIPE_PAIR_SPAWN_POSITION,
                constants::HIGH_PIPE_PAIR_SPAWN_POSITION,
            );
            let new_position = Position::new(constants::SCREEN_WIDTH as f32, new_position);
            position.insert(new_pipe_pair, new_position).unwrap();

            ticks_to_next_pipe_pair.0 = constants::TICKS_TO_NEXT_PIPE_PAIR;
        }
        ticks_to_next_pipe_pair.0 -= 1;
    }
}
impl<'a> System<'a> for SystemDeletePipesIfOffScreen {
    type SystemData = (Entities<'a>, ReadStorage<'a, Position>);
    fn run(&mut self, (entities, positions): Self::SystemData) {
        for (entity, position) in (&entities, &positions).join() {
            if (position.0).x <= -constants::PIPE_PAIR_WIDTH as f32 {
                entities.delete(entity).unwrap();
            }
        }
    }
}
impl<'a> System<'a> for SystemBumpPipePair {
    type SystemData = (
        ReadStorage<'a, Position>,
        Read<'a, PlayerPosition>,
        Write<'a, GameOver>,
    );
    fn run(
        &mut self,
        (position_read, player_position_resource, mut game_over_resource): Self::SystemData,
    ) {
        let collision = position_read
            .join()
            .map(|position| {
                let player_west_of_pipe_pair =
                    (player_position_resource.0).0.x + constants::PLAYER_WIDTH < position.0.x;

                let player_east_of_pipe_pair =
                    position.0.x + constants::PIPE_PAIR_WIDTH < (player_position_resource.0).0.x;

                let collide_x = !(player_west_of_pipe_pair || player_east_of_pipe_pair);

                let player_under_upper_pipe = position.0.y < (player_position_resource.0).0.y;

                let player_above_lower_pipe = (player_position_resource.0).0.y
                    + constants::PLAYER_HEIGHT
                    < position.0.y + constants::PIPE_PAIR_DISTANCE;

                let collide_y = !(player_under_upper_pipe && player_above_lower_pipe);

                debug_value!("player_west_of_pipe_pair: {:?}", player_west_of_pipe_pair);
                debug_value!("player_east_of_pipe_pair: {:?}", player_east_of_pipe_pair);
                debug_value!("collide_x: {:?}", collide_x);

                debug_value!("player_under_upper_pipe: {:?}", player_under_upper_pipe);
                debug_value!("player_above_lower_pipe: {:?}", player_above_lower_pipe);
                debug_value!("collide_y: {:?}", collide_y);

                collide_x && collide_y
            })
            .any(|x| x == true);

        if collision {
            game_over_resource.0 = true;
        }
    }
}
impl<'a> System<'a> for SystemBumpScreenEdge {
    type SystemData = (Read<'a, PlayerPosition>, Write<'a, GameOver>);
    fn run(&mut self, (player_position_resource, mut game_over_resource): Self::SystemData) {
        let player_position = &player_position_resource.0;

        let hit_top = player_position.0.y <= 0.;
        let hit_bottom =
            player_position.0.y + constants::PLAYER_HEIGHT >= constants::SCREEN_HEIGHT as f32;

        if hit_top || hit_bottom {
            game_over_resource.0 = true;
        }
    }
}
impl<'a> System<'a> for SystemPlayerJump {
    type SystemData = (
        Read<'a, JumpIsDown>,
        Write<'a, TicksToNextJump>,
        Read<'a, GameOver>,
        Write<'a, PlayerVelocity>,
    );
    fn run(
        &mut self,
        (jump_is_down, mut ticks_to_next_jump, game_over, mut player_velocity): Self::SystemData,
    ) {
        if jump_is_down.0 && ticks_to_next_jump.0 <= 0 && game_over.0 == false {
            player_velocity.0 = -constants::PLAYER_VELOCITY;
            ticks_to_next_jump.0 = constants::TICKS_TO_NEXT_JUMP;
        }
        ticks_to_next_jump.0 -= 1;
    }
}
impl<'a> System<'a> for SystemPlayerGravity {
    type SystemData = (Write<'a, PlayerVelocity>);
    fn run(&mut self, mut player_velocity: Self::SystemData) {
        player_velocity.0 += constants::GRAVITY;
    }
}
impl<'a> System<'a> for SystemPlayerVelocity {
    type SystemData = (Read<'a, PlayerVelocity>, Write<'a, PlayerPosition>);
    fn run(&mut self, (player_velocity, mut player_position): Self::SystemData) {
        (player_position.0).0 += quicksilver::geom::Vector::new(0, player_velocity.0);
    }
}